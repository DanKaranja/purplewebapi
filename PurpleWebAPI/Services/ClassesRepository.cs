﻿using PurpleWebAPI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PurpleWebAPI.Services
{
    public class ClassesRepository
    {
        private SqlConnection connection;

        public ClassesRepository()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NMMUPOSTGRADDB"].ConnectionString);

        }

        public ArrayList GetClasses(String UserID)
        {
            string SQLStr = "SELECT [Class].classID,[Class].classCode,[Class].classInstitution,[Class].classDescription FROM [Class-User],[Class] WHERE [Class-User].classID = [Class].classID AND [Class-User].userID = '"+ UserID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            ArrayList classList = new ArrayList();
            try
            {
                while (reader.Read())
                {
                    Classes rClass = new Classes();
                    rClass.ClassID = reader[0].ToString();
                    rClass.ClassName = reader[1].ToString();
                    rClass.ClassInstitution = reader[2].ToString();
                    rClass.ClassDescription = reader[3].ToString();
                    rClass.CountUserIDs = GetClassUserCount(rClass.ClassID);
                    classList.Add(rClass);
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return classList;
        }
        public ArrayList GetUsers(String ClassID)
        {
            string SQLStr = "SELECT [User].userID,[User].fullName,[User].cellNumber,[User].Karma FROM [User],[Class-User] WHERE [User].userID = [Class-User].[userID] AND [Class-User].classID = '" + ClassID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            ArrayList Users = new ArrayList();
            try
            {
                while (reader.Read())
                {
                    User rUser = new User();
                    rUser.UserID = reader[0].ToString();
                    rUser.FullName = reader[1].ToString();
                    rUser.CellNumber = reader[2].ToString();
                    rUser.Karma = int.Parse(reader[3].ToString());
                    Users.Add(rUser);
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return Users;
        }
        private String GetClassUserCount(String ClassID)
        {
            String count = "0";
            string SQLStr = "SELECT COUNT(*) FROM [Class-User],[Class] WHERE [Class-User].classID = [Class].classID AND [Class].classID = '"+ ClassID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                    count = reader[0].ToString();

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
            }
            return count;
        }
        public PResponse CheckClassExisting(String CCode, String Institution)
        {
            PResponse response = null;
            string SQLStr = "SELECT * FROM [Class] WHERE CONVERT(VARCHAR,classCode) = '" + CCode + "' AND  CONVERT(VARCHAR, classInstitution) = '" + Institution + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read()) 
                    response = new PResponse(1,reader[0].ToString());
                else
                    response = new PResponse(0,"The requested class does not exist, please go ahead and create it");

            }
            catch (Exception e) {
                response = new PResponse(0,e.Message);
            }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return response;
        }
        public PResponse AddClass(Classes newClass)
        {
            PResponse response = null;
            String SQLStr = "INSERT INTO [Class] VALUES ('" + newClass.ClassID + "','" + newClass.ClassName + "','" + newClass.ClassInstitution + "','" + newClass.ClassDescription + "');";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                response = new PResponse(1, "Successfully added " + newClass.ClassName);
            }
            catch (Exception e)
            {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }

            return response;
        }
        public ArrayList GetUserIDs(String ClassID)
        {
            ArrayList Users = new ArrayList();
            String SQLStr = "SELECT [userID] FROM [Class-User] WHERE classID = '" + ClassID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    Users.Add(reader[0].ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return Users;
        }
        public bool CheckUsuer_ClassExisting(String UserID, String ClassID)
        {
            string SQLStr = "SELECT * FROM [Class-User] WHERE userID = '" + UserID + "' AND classID = '" + ClassID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                    return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return false;
        }
        public PResponse AddUserToClass(String UserID, String ClassID)
        {
            PResponse response = null;
            String SQLStr = "INSERT INTO [Class-User] VALUES ('" + ClassID + "','" + UserID + "');";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                response = new PResponse(1, "Successfully added you to class");
            }
            catch (Exception e)
            {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }

            return response;
        }
        public PResponse RemoveUserFromRecords(String UserID)
        {
            PResponse response = null;
            String SQLStr = "DELETE FROM [Class-User] WHERE userID = '" + UserID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                response = new PResponse(1, null);
            }
            catch (Exception e)
            {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }
            return response;
        }
        public PResponse RemoveUserFromClass(String UserID, String ClassID)
        {
            PResponse response = null;
            String SQLStr = "DELETE FROM [Class-User] WHERE userID = '" + UserID + "' AND classID = '" + ClassID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                response = new PResponse(1, "You were successfully removed from that class");
            }
            catch (Exception e)
            {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }
            return response;
        }



    }
}