﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using PurpleWebAPI.Models;
using System.Collections;
using System.Data;

namespace PurpleWebAPI.Services
{
    public class ChatRepository
    {
        private SqlConnection connection;
        private ClassesRepository ClassDBRepo;
        private UserRepository UserDBRepo;

        public ChatRepository()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NMMUPOSTGRADDB"].ConnectionString);
            ClassDBRepo = new ClassesRepository();
            UserDBRepo = new UserRepository();
        }

        public PResponse AddChatMessage(ChatMessage newMessage)
        {
            PResponse response = null;
            String SQLStr = "INSERT INTO [Chats] VALUES ('" + newMessage.MessageID + "','" + newMessage.Message + "','" + newMessage.UserID + "','" + newMessage.HotspotID + "','" + newMessage.CreationDate + "','" + DateTime.Now + "','" + newMessage.Type + "');";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                response = new PResponse(1, "Successfully added message by " + newMessage.UserID + " on hotspot " + newMessage.HotspotID + ", initially created at " + newMessage.CreationDate);
            }
            catch (Exception e)
            {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }
            return response;
        }
        public bool AddVote(String[] Vote)
        {
            bool response = false;
            String SQLStr = "INSERT INTO [Votes] VALUES ('" + Vote[0] + "','" + Vote[1] + "','" + Vote[2] + "','" + Vote[3] + "');";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                response = true;
            }
            catch (Exception e)
            {        
            }
            finally
            {
                connection.Close();
            }
            return response;
        }
        public bool CheckExistingVote(String ChatID,String VoterID, int Vote)
        {
            bool response = false;
            string SQLStr = "SELECT * FROM [Votes] WHERE voterID = '" + VoterID + "' AND chatID = '" + ChatID + "' AND vote = '" + Vote + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                    response = true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return response;
        }

      
        public PResponse SendKarma(String VoteeID, int Karma)
        {
            int CurrentKarma = UserDBRepo.GetUserKarma(VoteeID);
            PResponse response = null;
            String SQLStr = "UPDATE [User] SET Karma = '"+ (CurrentKarma + Karma) + "' WHERE userID = '"+ VoteeID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                response = new PResponse(1, "Yum");
            }
            catch (Exception e)
            {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }
            return response;
        }

        public ArrayList GetAllHotspotMessages(String hotspotID)
        {
            string SQLStr = "SELECT * FROM [Chats] WHERE hotspotID = '" + hotspotID + "' ORDER BY recievedDate ASC;";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            ArrayList ChatMessages = new ArrayList();
            try
            {
                while (reader.Read())
                {
                    ChatMessage message = new ChatMessage();
                    message.MessageID = reader[0].ToString();
                    message.Message = reader[1].ToString();
                    message.UserID = reader[2].ToString();
                    message.HotspotID = reader[3].ToString();
                    message.CreationDate = reader[4].ToString();
                    message.Type = int.Parse(reader[6].ToString());
                    message.FromUser = UserDBRepo.GetUser(message.UserID, 0);
                    ChatMessages.Add(message);
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return ChatMessages;
        }

        public ArrayList GetNewHotspotMessages(String hotspotID, String lastMessageCreationDate)
        {
            string SQLStr = "SELECT * FROM [Chats] WHERE hotspotID = '" + hotspotID + "' AND creationDate > " + lastMessageCreationDate + " ORDER BY creationDate ASC;";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            ArrayList ChatMessages = new ArrayList();
            try
            {
                while (reader.Read())
                {
                    ChatMessage message = new ChatMessage();
                    message.MessageID = reader[0].ToString();
                    message.Message = reader[1].ToString();
                    message.UserID = reader[2].ToString();
                    message.HotspotID = reader[3].ToString();
                    message.CreationDate = reader[4].ToString();
                    ChatMessages.Add(message);
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return ChatMessages;
        }


    }
}