﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using PurpleWebAPI.Models;

namespace PurpleWebAPI.Services
{
    public class UserRepository
    {
        private SqlConnection connection;

        public UserRepository()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NMMUPOSTGRADDB"].ConnectionString);

        }
        public ArrayList AllUsers()
        {
            string SQLStr = "SELECT * FROM [User];";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            ArrayList Users = new ArrayList();
            try
            {
                while (reader.Read())
                {
                    User user = new User();
                    user.UserID = reader[0].ToString();
                    user.FullName = reader[1].ToString();
                    Users.Add(user);
                }
    
            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return Users;
           

        }
        public int GetUserKarma(String UserID)
        {
            int Karma = 0;
            string SQLStr = "SELECT Karma FROM [User] WHERE userID = '" + UserID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            User user = new User();
            try
            {
                while (reader.Read())
                {
                    Karma = Int32.Parse(reader[0].ToString());
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return Karma;

        }
        public User GetUser(String FullName, String CellNumber)
        {
            string SQLStr = "SELECT * FROM [User] WHERE CONVERT(VARCHAR, fullName) = '" + FullName + "' AND  CONVERT(VARCHAR, cellNumber) = '" + CellNumber + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            User user = new User();
            try
            {
                while (reader.Read())
                {
                    user.UserID = reader[0].ToString();
                    user.FullName = reader[1].ToString();
                    user.CellNumber = reader[2].ToString();
                    user.Karma = Int32.Parse(reader[3].ToString());
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return user;
        }
        public User GetUser(String userID, int State)
        {
            string SQLStr = "SELECT * FROM [User] WHERE userID = '"+userID+"';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            User user = new User();
            try
            {
                while (reader.Read())
                {
                    user.UserID = reader[0].ToString();
                    user.FullName = reader[1].ToString();
                    user.CellNumber = reader[2].ToString();
                    user.Karma = Int32.Parse(reader[3].ToString());
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return user;
        }
        public bool CheckExisting(String CellNumber)
        {
            string SQLStr = "SELECT * FROM [User] WHERE CONVERT(VARCHAR, cellNumber) = '" + CellNumber + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                    return true;

            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return false;
        }

        public PResponse AddUser(User user)
        {
            PResponse response = null;
            if (!CheckExisting(user.CellNumber))
            {
                String SQLStr = "INSERT INTO [User] VALUES ('" + user.UserID + "','" + user.FullName + "','" + user.CellNumber + "','" + user.Karma + "');";
                SqlCommand command = new SqlCommand(SQLStr, connection);
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    response = new PResponse(1, "Successfully registered " + user.FullName);
                }
                catch (Exception e)
                {
                    response = new PResponse(0, e.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            else
                response = new PResponse(0, "A user already exists with Cell number "+user.CellNumber);

            return response;
        }
        public PResponse DeleteUser(String userID)
        {
            PResponse response = null;
            String SQLStr = "DELETE FROM [User] WHERE userID = '" + userID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                response = new PResponse(1,null);
            }
            catch (Exception e) {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }
            return response;
        }

    }
}