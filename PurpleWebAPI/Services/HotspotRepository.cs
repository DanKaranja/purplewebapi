﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using PurpleWebAPI.Models;
using System.Collections;

namespace PurpleWebAPI.Services
{
    public class HotspotRepository
    {
        private SqlConnection connection;
        private ClassesRepository ClassDBRepo;

        public HotspotRepository()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NMMUPOSTGRADDB"].ConnectionString);
            ClassDBRepo = new ClassesRepository();
        }

        public PResponse AddHotspot(Hotspot newHotspot)
        {
            PResponse response = null;
            String SQLStr = "INSERT INTO [Hotspots] VALUES ('" + newHotspot.HotspotID + "','" + newHotspot.HotspotQuestion + "','" + newHotspot.HotspotDescription + "','" + newHotspot.HotspotAdminUserID + "','" + newHotspot.HotspotCreationDate + "');";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                foreach(String eachClass in newHotspot.HotspotClassIDs)
                {
                    //Adding to Hotspot-Class
                    AddHotspot_Class(newHotspot.HotspotID, eachClass);
                    //Adding Admin to hotspot
                    AddHotspot_User(newHotspot.HotspotID, newHotspot.HotspotAdminUserID,0);
                    //Adding classes users to hotspot
                    foreach(String eachUser in ClassDBRepo.GetUserIDs(eachClass))
                    {
                        AddHotspot_User(newHotspot.HotspotID, eachUser,0);
                    }
                }
                response = new PResponse(1, "Successfully added '" + newHotspot.HotspotQuestion+"'");

            }
            catch (Exception e)
            {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }

            return response;
        }
        public bool AddHotspot_User(String HotspotID, String UserID,int State)
        {
            bool response = false;
            if(State == 1)
            {
                connection.Open();
            }
            string SQLCheck = "SELECT * FROM [Hotspots-Users] WHERE hotspotID = '" + HotspotID + "' AND userID = '" + UserID + "';";
            SqlCommand checkCommand = new SqlCommand(SQLCheck, connection);
            SqlDataReader reader = checkCommand.ExecuteReader();
            try
            {
                if (reader.Read())
                    response = false;
                else
                {
                    String SQLStr = "INSERT INTO [Hotspots-Users] VALUES ('" + HotspotID + "','" + UserID + "');";
                    SqlCommand insertCommand = new SqlCommand(SQLStr, connection);
                    insertCommand.ExecuteNonQuery();
                    response = true;
                }
            }
            catch (Exception e)
            {
                
                response = false;
            }
            finally
            {
                if (State == 1)
                {
                    connection.Close();
                }
                reader.Close();
            }
            return response;
        }
        private bool AddHotspot_Class(String HotspotID, String ClassID)
        {
            bool response = false;
            string SQLCheck = "SELECT * FROM [Class-Hotspot] WHERE hotspotID = '" + HotspotID + "' AND classID = '" + ClassID + "';";
            SqlCommand checkCommand = new SqlCommand(SQLCheck, connection);
            SqlDataReader reader = checkCommand.ExecuteReader();
            try
            {
                if (reader.Read())
                    response = false;
                else
                {
                    String SQLStr = "INSERT INTO [Class-Hotspot] VALUES ('" + HotspotID + "','" + ClassID + "');";
                    SqlCommand insertCommand = new SqlCommand(SQLStr, connection);
                    insertCommand.ExecuteNonQuery();
                    response = true;
                }
            }
            catch (Exception e)
            {
                response = false;
            }
            return response;
        }
        public ArrayList GetUsers(String hotspotID)
        {
            string SQLStr = "SELECT [User].userID,[User].fullName,[User].cellNumber,[User].Karma FROM [User],[Hotspots-Users] WHERE [User].userID = [Hotspots-Users].[userID] AND [Hotspots-Users].hotspotID = '" + hotspotID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            ArrayList Users = new ArrayList();
            try
            {
                while (reader.Read())
                {
                    User rUser = new User();
                    rUser.UserID = reader[0].ToString();
                    rUser.FullName = reader[1].ToString();
                    rUser.CellNumber = reader[2].ToString();
                    rUser.Karma = int.Parse(reader[3].ToString());
                    Users.Add(rUser);
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return Users;
        }
        public ArrayList GetUserHotspots(String userID)
        {
            string SQLStr = "SELECT [Hotspots].hotspotID,[Hotspots].hotspotQuestion,[Hotspots].hotspotDescription,[Hotspots].hotspotAdminUserID,[Hotspots].hotspotCreationDate FROM [Hotspots],[Hotspots-Users] WHERE [Hotspots].hotspotID = [Hotspots-Users].[hotspotID] AND [Hotspots-Users].userID = '"+ userID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            ArrayList Hotspots = new ArrayList();
            try
            {
                while (reader.Read())
                {
                    Hotspot rhotspot = new Hotspot();
                    rhotspot.HotspotID = reader[0].ToString();
                    rhotspot.HotspotQuestion = reader[1].ToString();
                    rhotspot.HotspotDescription = reader[2].ToString();
                    rhotspot.HotspotAdminUserID = reader[3].ToString();
                    rhotspot.HotspotCreationDate = reader[4].ToString();
                    rhotspot.HotspotClassIDs = GetHotspotClasses(rhotspot.HotspotID,0);
                    Hotspots.Add(rhotspot);
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                reader.Close();
                connection.Close();
            }
            return Hotspots;
        }
        public ArrayList GetHotspotClasses(String ID,int State)
        {
            string SQLStr;
            switch (State)
            {
                case 1:
                    connection.Open();
                    SQLStr = "SELECT [hotspotID] FROM [Class-Hotspot] WHERE classID = '" + ID + "';";
                    break;
                default:
                    SQLStr = "SELECT [classID] FROM [Class-Hotspot] WHERE hotspotID = '" + ID + "';";
                    break;
            }
            
            SqlCommand command = new SqlCommand(SQLStr, connection);
            SqlDataReader reader = command.ExecuteReader();
            ArrayList List = new ArrayList();
            try
            {
                while (reader.Read())
                {
                    List.Add(reader[0].ToString());
                }

            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            finally
            {
                if (State != 0)
                    connection.Close();
                reader.Close();
            }
            return List;
        }

        public PResponse RemoveUserFromRecords(String UserID)
        {
            PResponse response = null;
            String SQLHUStr = "DELETE FROM [Hotspots-Users] WHERE userID = '" + UserID + "';";
            String SQLHStr = "DELETE FROM [Hotspots] WHERE hotspotAdminUserID = '" + UserID + "';";
            SqlCommand HUcommand = new SqlCommand(SQLHUStr, connection);
            SqlCommand Hcommand = new SqlCommand(SQLHStr, connection);
            connection.Open();
            try
            {
                HUcommand.ExecuteNonQuery();
                Hcommand.ExecuteNonQuery();
                response = new PResponse(1, null);
            }
            catch (Exception e)
            {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }
            return response;
        }

        public PResponse RemoveUserFromHotspot(String HotspotID, String UserID)
        {
            PResponse response = null;
            String SQLStr = "DELETE FROM [Hotspots-Users] WHERE hotspotID = '" + HotspotID + "' AND userID = '" + UserID + "';";
            SqlCommand command = new SqlCommand(SQLStr, connection);
            connection.Open();
            try
            {
                command.ExecuteNonQuery();
                response = new PResponse(1, "You were successfully removed from the Hotspot");
            }
            catch (Exception e)
            {
                response = new PResponse(0, e.Message);
            }
            finally
            {
                connection.Close();
            }
            return response;

        }
    }
}