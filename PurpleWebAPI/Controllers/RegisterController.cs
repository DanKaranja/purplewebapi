﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class RegisterController : ApiController
    {
        private UserUtilities utilities;
        public RegisterController()
        {
            utilities = new UserUtilities();
        }

        // POST: api/Register
        public PResponse Post([FromBody]User value)
        {
            return utilities.RegisterUser(value);
        }

    }
}
