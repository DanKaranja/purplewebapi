﻿using PurpleWebAPI.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class GetHotspotsController : ApiController
    {
        private HotspotUtilities utilities;
        public GetHotspotsController()
        {
            utilities = new HotspotUtilities();
        }

        // GET: api/GetHotspots/5
        public ArrayList Get(String id)
        {
            if (id != null)
            {
                return utilities.GetHotspots(id);
            }
            return null;
        }

    }
}
