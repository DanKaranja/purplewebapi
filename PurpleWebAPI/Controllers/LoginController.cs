﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class LoginController : ApiController
    {
        private UserUtilities utilities;
        public LoginController()
        {
            utilities = new UserUtilities();
        }

        // POST: api/Login
        public User Post([FromBody]String[] value)
        {
            if(value != null) { 
            return utilities.Login(value[0], value[1]);
            }
            return null; 
        }

    }
}
