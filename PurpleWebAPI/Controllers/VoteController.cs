﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class VoteController : ApiController
    {
        private ChatUtilities utilities;
        public VoteController()
        {
            utilities = new ChatUtilities();
        }

        public PResponse Post([FromBody]String[] value)
        {
            if(value != null)
                return utilities.PostVote(value);
            else
                return new PResponse(0, "Invalid Parameters");
        }

    }
}
