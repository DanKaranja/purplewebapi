﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class AddClassController : ApiController
    {
        private ClassUtilities utilities;
        public AddClassController()
        {
            utilities = new ClassUtilities();
        }
    
        public PResponse Post([FromBody]Classes value)
        {
            if (value != null)
            {
                return utilities.AddClass(value);
            }return new PResponse(0, "Invalid Parameters"); 
        }

      
    }
}
