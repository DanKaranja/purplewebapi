﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class LeaveHotspotController : ApiController
    {
        private HotspotUtilities utilities;
        public LeaveHotspotController()
        {
            utilities = new HotspotUtilities();
        }


        // POST: api/LeaveHotspot
        public PResponse Post([FromBody]String[] value)
        {
            if (value != null)
            {
                return utilities.RemoveUserFromHotspot(value[0], value[1]);
            }
            return new PResponse(0, "Invalid Parameters");
        }

    }
}
