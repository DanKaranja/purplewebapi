﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class CreateHotspotController : ApiController
    {
        private HotspotUtilities utilities;
        public CreateHotspotController()
        {
            utilities = new HotspotUtilities();
        }


        // POST: api/CreateHotspot
        public PResponse Post([FromBody]Hotspot value)
        {
            if (value != null)
            {
                return utilities.AddHotspot(value);
            }
            return new PResponse(0, "Invalid Parameters"); ;
        }


    }
}
