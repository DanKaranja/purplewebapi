﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class SendMsgController : ApiController
    {
        private ChatUtilities utilities;
        public SendMsgController()
        {
            utilities = new ChatUtilities();
        }

        // POST: api/SendMsg
        public PResponse Post([FromBody]ChatMessage value)
        {
            if (value != null)
            {
                return utilities.PushMessage(value);
            }
            return new PResponse(0, "Invalid Parameters");
        }


    }
}
