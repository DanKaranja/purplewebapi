﻿using PurpleWebAPI.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class GetAllMsgController : ApiController
    {
        private ChatUtilities utilities;
        public GetAllMsgController()
        {
            utilities = new ChatUtilities();
        }

        // GET: api/GetAllMsg/5
        public ArrayList Get(String id)
        {
            if (id != null)
            {
                return utilities.GetAllMessages(id);
            }
            return null;
        }

    }
}
