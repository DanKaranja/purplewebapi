﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class SearchClassController : ApiController
    {
        private ClassUtilities utilities;
        public SearchClassController()
        {
            utilities = new ClassUtilities();
        }

        public ArrayList Get(String id)
        {
           return utilities.GetUserClasses(id);
        }

        // POST: api/Class
        public PResponse Post([FromBody]String[] value)
        {
            if(value != null) { 
            return utilities.ClassSearch(value[0], value[1]);
            }
            return new PResponse(0, "Invalid Parameters"); ;
        }

    }
}
