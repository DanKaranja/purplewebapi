﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class TestController : ApiController
    {
        // GET: api/Test
        public String Get()
        {
            return "API is up and running";
        }
        
    }
}
