﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class UserController : ApiController
    {
        private UserUtilities utilities;
        public UserController()
        {
            utilities = new UserUtilities();
        }

        // GET: api/User/'id'
        public User Get(String id)
        {
            return utilities.GetUser(id);
        }


    }
}
