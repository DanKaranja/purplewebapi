﻿using PurpleWebAPI.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class GetClassUsersController : ApiController
    {
        private ClassUtilities utilities;
        public GetClassUsersController()
        {
            utilities = new ClassUtilities();
        }

        // GET: api/GetClassUsers/5
        public ArrayList Get(String id)
        {
            if (id != null)
            {
                return utilities.GetUsers(id);
            }
            return null;
        }

    }
}
