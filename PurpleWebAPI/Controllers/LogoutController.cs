﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurpleWebAPI.Controllers
{
    public class LogoutController : ApiController
    {
        private UserUtilities utilities;
        public LogoutController()
        {
            utilities = new UserUtilities();
        }
        // GET: api/Logout
        public PResponse Get(String id)
        {
            return utilities.RemoveUser(id);
        }

    }
}
