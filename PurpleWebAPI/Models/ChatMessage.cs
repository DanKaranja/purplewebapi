﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpleWebAPI.Models
{
    //used both for recieving and sending
    public class ChatMessage
    {
        public String MessageID { get; set; }
        public String Message { get; set; }
        public String UserID { get; set; }
        public User FromUser { get; set; }
        public String HotspotID { get; set; }
        public String CreationDate { get; set; }
        public int Type { get; set; }

        public ChatMessage()
        {

        }

        public ChatMessage(String Message, String UserID, String HotspotID, String CreationDate, int Type)
        {
            this.MessageID = Guid.NewGuid().ToString();
            this.Message = Message;
            this.UserID = UserID;
            this.HotspotID = HotspotID;
            this.CreationDate = CreationDate;
            this.Type = Type;
        }
    }
}