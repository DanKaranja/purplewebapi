﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpleWebAPI.Models
{
    public class Classes
    {
        public String ClassID { get; set; }
        public String ClassName { get; set; }
        public String ClassInstitution { get; set; }
        public String ClassDescription { get; set; }
        public String CountUserIDs { get; set; }

        public Classes()
        {

        }
        public Classes(String ClassName, String Institution, String ClassDescription)
        {
            this.ClassID = Guid.NewGuid().ToString();
            this.ClassName = ClassName;
            this.ClassInstitution = Institution;
            this.ClassDescription = ClassDescription;
        }
    }
}