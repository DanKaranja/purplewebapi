﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpleWebAPI.Models
{
    public class User
    {
        public String FullName { get; set; }
        public String UserID { get; set; }
        public String CellNumber { get; set; }
        public int Karma { get; set; }
        public User() { }
        public User(String FullName,String CellNumber)
        {
            this.UserID = Guid.NewGuid().ToString();
            this.FullName = FullName;
            this.CellNumber = CellNumber;
            this.Karma = 0;

        }
    }
}