﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpleWebAPI.Models
{
    public class PResponse
    {
        public int check { get; set; }
        public String responseString { get; set; }

        public PResponse(int check, String responseString)
        {
            this.check = check;
            this.responseString = responseString;
        }
    }
}