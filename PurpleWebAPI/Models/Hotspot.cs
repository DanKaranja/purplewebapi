﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpleWebAPI.Models
{
    public class Hotspot
    {
        public String HotspotID { get; set; }
        public String HotspotQuestion { get; set; }
        public String HotspotDescription { get; set; }
        public String HotspotAdminUserID { get; set; }
        public String HotspotCreationDate { get; set; }
        public ArrayList HotspotClassIDs { get; set; }

        public Hotspot()
        {

        }

        public Hotspot(String Title,String Description, String AdminUserID, String CreationDate, ArrayList ClassIDs)
        {
            this.HotspotID = Guid.NewGuid().ToString();
            this.HotspotQuestion = Title;
            this.HotspotDescription = Description;
            this.HotspotAdminUserID = AdminUserID;
            this.HotspotCreationDate = CreationDate;
            this.HotspotClassIDs = ClassIDs;
        }
    }
}