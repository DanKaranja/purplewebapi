﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpleWebAPI.Utilities
{
    public class ClassUtilities
    {
        private UserRepository UserDBRepo;
        private ClassesRepository ClassDBRepo;
        private HotspotRepository HotspotDBRepo;
        private String GenericError = "Unknown error occured";

        public ClassUtilities()
        {
            UserDBRepo = new UserRepository();
            ClassDBRepo = new ClassesRepository();
            HotspotDBRepo = new HotspotRepository();
        }
        public PResponse RemoveUserFromClass(String UserID, String ClassID)
        {
            PResponse response = ClassDBRepo.RemoveUserFromClass(UserID, ClassID);
            if (response == null)
                response = new PResponse(0, GenericError);
            return response;
        }
        public ArrayList GetUsers(String ClassID)
        {
            return ClassDBRepo.GetUsers(ClassID);
        }
        public ArrayList GetUserClasses(String UserID)
        {
            return ClassDBRepo.GetClasses(UserID);
        }

        public PResponse ClassSearch(String CCode, String Institution)
        {
            PResponse response = ClassDBRepo.CheckClassExisting(CCode,Institution);
            if (response == null)
                response = new PResponse(0, GenericError);
            return response;
        }

        public PResponse AddClass(Classes newClass)
        {
            PResponse response = ClassDBRepo.AddClass(newClass);
            if (response == null)
                return new PResponse(0, GenericError);
            return response;
        }

        public PResponse AddUserToClass(String UserID, String ClassID)
        {
            PResponse response;
            if (ClassDBRepo.CheckUsuer_ClassExisting(UserID, ClassID))
                return new PResponse(0, "You are already part of that class");
            else
            {
                response = ClassDBRepo.AddUserToClass(UserID, ClassID);
                if (response != null)
                {
                    if (response.check != 0)
                    {
                        ArrayList hotspots = HotspotDBRepo.GetHotspotClasses(ClassID, 1);
                        foreach(String hotspot in hotspots)
                        {
                            HotspotDBRepo.AddHotspot_User(hotspot, UserID,1);
                        }
                        return new PResponse(1, "Successfully joined Class and all of its associated Hotspots.");
                    }
                }
            }           
            return new PResponse(0, GenericError);
        }

    }
}