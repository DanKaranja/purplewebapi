﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpleWebAPI.Utilities
{
    public class HotspotUtilities
    {
        private UserRepository UserDBRepo;
        private ClassesRepository ClassDBRepo;
        private HotspotRepository HotspotDBRepo;
        private String GenericError = "Unknown error occured";

        public HotspotUtilities()
        {
            UserDBRepo = new UserRepository();
            ClassDBRepo = new ClassesRepository();
            HotspotDBRepo = new HotspotRepository();
        }

        public PResponse AddHotspot(Hotspot newOne)
        {
            PResponse response = HotspotDBRepo.AddHotspot(newOne);
            if (response == null)
                response = new PResponse(0, GenericError);
            return response;
        }
        public ArrayList GetHotspots(String UserID)
        {
            return HotspotDBRepo.GetUserHotspots(UserID);
        }
        public ArrayList GetUsers(String HotspotID)
        {
            return HotspotDBRepo.GetUsers(HotspotID);
        }
        public PResponse RemoveUserFromHotspot(String HotspotID, String UserID)
        {
            PResponse response = HotspotDBRepo.RemoveUserFromHotspot(HotspotID,UserID);
            if (response == null)
                response = new PResponse(0, GenericError);
            return response;
        }
    }
   
}