﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PurpleWebAPI.Models;
using PurpleWebAPI.Services;

namespace PurpleWebAPI.Utilities
{
    public class UserUtilities
    {
        private UserRepository UserDBRepo;
        private ClassesRepository ClassDBRepo;
        private HotspotRepository HotspotDBRepo;
        private String GenericError = "Unknown error occured";

        public UserUtilities()
        {
            //Use this object to add, delete and get users from the database.
            ClassDBRepo = new ClassesRepository();
            UserDBRepo = new UserRepository();
            HotspotDBRepo = new HotspotRepository();
        }
        public User GetUser(String UserID)
        {
            return UserDBRepo.GetUser(UserID,1);
        }

        public User Login(String FullName, String cellnumber)
        {
            return UserDBRepo.GetUser(FullName, cellnumber); 
        }

        public PResponse RegisterUser(User newone)
        {
            PResponse response = UserDBRepo.AddUser(newone);
            if(response == null)
                response = new PResponse(0, GenericError);

            return response;
        }
        public PResponse RemoveUser(String userID)
        {
            PResponse ClassRepoResponse = ClassDBRepo.RemoveUserFromRecords(userID);
            PResponse HotspotReopResponse = HotspotDBRepo.RemoveUserFromRecords(userID);
            PResponse UserRepoResponse = UserDBRepo.DeleteUser(userID);
            if ((UserRepoResponse.check != 1) || (ClassRepoResponse.check != 1) || (HotspotReopResponse.check != 1))
                return new PResponse(0, GenericError);
            else
                return new PResponse(1,"Account and records were successfully removed"); ;
        }
    }
}