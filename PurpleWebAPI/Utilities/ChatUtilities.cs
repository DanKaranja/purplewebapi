﻿using PurpleWebAPI.Models;
using PurpleWebAPI.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpleWebAPI.Utilities
{
    public class ChatUtilities
    {
        private UserRepository UserDBRepo;
        private ClassesRepository ClassDBRepo;
        private ChatRepository ChatDBRepo;
        private String GenericError = "Unknown error occured";

        public ChatUtilities()
        {
            UserDBRepo = new UserRepository();
            ClassDBRepo = new ClassesRepository();
            ChatDBRepo = new ChatRepository();
        }

        public PResponse PushMessage(ChatMessage Message)
        {
            PResponse response = ChatDBRepo.AddChatMessage(Message);
            if (response == null)
                response = new PResponse(0, GenericError);
            return response;
        }

        public ArrayList GetAllMessages(String HotspotID)
        {
            return ChatDBRepo.GetAllHotspotMessages(HotspotID);
        }

        public PResponse PostVote(String[] VoteDetails)
        {
            if (ChatDBRepo.CheckExistingVote(VoteDetails[0], VoteDetails[1], Int32.Parse(VoteDetails[3])))
            {
                return new PResponse(0, "Mate, you already voted");
            }
            else
            {
                if (ChatDBRepo.AddVote(VoteDetails))
                {
                    return ChatDBRepo.SendKarma(VoteDetails[2], Int32.Parse(VoteDetails[3]));
                }
                return new PResponse(0, "Nope, Something went wrong");

            }
        }
    }
}